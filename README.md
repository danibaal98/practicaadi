# Práctica ADI

Práctica de la asignatura Aplicaciones Distribuidas en Internet de la UCLM

## Integrantes y rol
- Maria Espinosa Astilleros 15433216D: Backend
- Daniel Ballesteros Almazán 71357850M: Red blockchain
- Mario Muñoz Jiménez 05722538T: Frontend

## 3rd parties utilizadas
Las 3rd parties utilizadas son las siguientes:
- VirtualBox 6.0.14
- Minikube/Kubectl
- pyWeb3
- bootnode
- geth
- constellation
- truffle
- ganache-cli

## Instrucciones de despliegue
Para despelgar la red de blockchain, consultar el README.rd de la carpeta **qubernetes3nodes**. Una vez que ya tiene desplegada la red, hay que compilar el Smart Contract. Primero, nos movemos a la carpeta de **despliegue_dapp**, y después usamos el siguiente comando:
```bash
truffle compile
```
Después, en un terminal aparte, se deja ejecutando el siguiente comando:
```bash
geth --rpc
```
Mientras esta corriendo, en otro terminal se ejecuta:
```bash
truffle migrate
```
Para ejecutar el frontend, hay que ejecutar:
```bash
python3 Votaciones.py
```